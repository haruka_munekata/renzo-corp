<?php get_header(); ?>
	<?php
	global $wp_query;
	query_posts(array_merge(
	    array( 'post_type' => array('post','blog') ),
	    $wp_query->query
	));
	?>
                <section class="sec01">
                    <h2 class="ttl02"><?php post_type_archive_title(); ?></h2>
                    <p class="mb2">renzoからのお知らせや、最新情報をお届けいたします。</p>
                    <div>
                        <?php if(have_posts()):while(have_posts()):the_post();?>
                        <article class="archive-blogarea">
                            <a href="<?php the_permalink();?>">
			<span class="imgdiv">
			    <?php if (has_post_thumbnail()) : {the_post_thumbnail( 'full' );}?>
			    <?php else : ?>
			    <img src="http://dummyimage.com/400x600/ccc/999" alt="">
			    <?php endif; ?>
			</span>
			<div>
			    <span class="bdl">
			        <time><?php the_time('Y/m/d'); ?></time>
			        <h3 class="ttl03"><?php echo get_the_title();?></h3>
			        <p><?php the_field( 'more' ); ?></p>
			    </span>
			</div>
                            </a>
                        </article>
                        <?php endwhile;?>
                        <?php endif;?>
                    </div>
                    </section>
		<?php
		$args = array(
		'next_text' => '&lt; 次の記事を読む',
		'prev_text' => '前の記事を読む &gt;',
		'screen_reader_text' => 'ページナビゲーション'
		);
		the_posts_navigation($args);
		?>
		</main>
<?php get_footer(); ?>