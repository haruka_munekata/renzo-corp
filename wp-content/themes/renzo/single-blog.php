<?php get_header(); ?>
                <?php
                global $wp_query;
                query_posts(array_merge(
                    array( 'post_type' => array('post','blog') ),
                    $wp_query->query
                ));
                if (have_posts()) : while (have_posts()) :
                the_post();
                ?>
                <article class="sec01">
                    <time><?php the_time('Y/m/d'); ?></time>
                    <h2 class="ttl02"><?php the_title(); ?></h2>
                    <div class="eyecatch">
                        <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail(); ?>
                        </a>
                        <?php else : ?>
                        <img src="http://dummyimage.com/840x480/ccc/999" alt="">
                        <?php endif; ?>
                    </div>
                </article>
                <article class="sec02">
                    <p><?php the_content(); ?></p>
                </article>
                <?php
                endwhile;
                endif;
                ?>
                </main>
<?php get_footer(); ?>