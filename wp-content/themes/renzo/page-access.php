<?php get_header(); ?>
                <article class="sec01">
                    <h2 class="ttl02"><?php echo get_the_title(); ?></h2>
                        <dl class="list02">
                        	<dt>所在地</dt>
                        	<dd>〒248-0006 神奈川県鎌倉市小町2-13-1</dd>
                        	<dt>電話番号</dt>
                        	<dd>0467-23-7817</dd>
                        	<dt>営業時間</dt>
                        	<dd>11:00〜18:00</dd>
                        	<dt>定休日</dt>
                        	<dd class="mb2">無休</dd>
                        </dl>
                        <div class="map">
                        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.3829759949144!2d139.55159761523672!3d35.32131015723414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601845c6b438a431%3A0x4b65b83221af7df5!2z44CSMjQ4LTAwMDYg56We5aWI5bed55yM6Y6M5YCJ5biC5bCP55S677yS5LiB55uu77yR77yT4oiS77yR!5e0!3m2!1sja!2sjp!4v1446112009312" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                </article>
<?php get_footer(); ?>