<?php get_header(); ?>
	<?php
	global $wp_query;
	query_posts(array_merge(
	    array( 'post_type' => array('post','collections') ),
	    $wp_query->query
	));
	?>
                <section class="sec01">
                    <h2 class="ttl02"><?php post_type_archive_title(); ?></h2>
                    <p class="mb2">renzoの商品の紹介や、こだわりをご覧いただけます。</p>
                    <div class="pickupcontents">
		<?php
		if (have_posts()) : while (have_posts()) :
		the_post();
		?>
                        <div>
                            <a href="<?php the_permalink(); ?>">
				<?php if (has_post_thumbnail()) : {the_post_thumbnail( 'full' );}?>
				<?php else : ?>
				<img src="http://dummyimage.com/340x400/ccc/999" alt="">
				<?php endif; ?>
                            </a>
                            <div class="cl-effect-1">
                                <h3 class="ttl04"><?php the_title(); ?></h3>
                                <p><?php the_field( 'overview' ); ?></p>
                                <a href="<?php the_permalink(); ?>" class="btn02 sc">MORE</a>
                            </div>
                        </div>
		<?php
		endwhile;
		endif;
		?>
                    </div>
                    </section>
		<?php
		$args = array(
		'next_text' => '&lt; Prev',
		'prev_text' => 'Next &gt;',
		'screen_reader_text' => 'ページナビゲーション'
		);
		the_posts_navigation($args);
		?>
<?php get_footer(); ?>