            <?php if(! is_post_type_archive( 'blog' ) && ! is_singular('blog')) : ?>
            </main>
            <?php endif; ?>
            <footer id="footer">
                <div class="cta"><a href="https://www.facebook.com/pages/Renzo-%E9%8E%8C%E5%80%89/515917995225706" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/common/btn_fb.png" alt="facebookへのリンク"></a></div>
                <ul id="f-nav">
                    <li><a href="<?php echo home_url(); ?>/">HOME</a></li>
                    <li><a href="<?php echo home_url(); ?>/blog/">BLOG</a></li>
                    <li><a href="<?php echo home_url(); ?>/collections/">COLLECTIONS</a></li>
                    <li><a href="<?php echo home_url(); ?>/access/">ACCESS</a></li>
                    <li><a href="<?php echo home_url(); ?>/company/">COMPANY</a></li>
                    <li><a href="<?php echo home_url(); ?>/privacy/">PRIVACY</a></li>
                    <li><a href="<?php echo home_url(); ?>/sitemap/">SITEMAP</a></li>
                </ul>
                <small>©2002 Ventomarino co.,ltd.</small>
            </footer>
            <!-- /#footer -->
            </div>
        </div>
        <!-- /#wrapper -->
        <div id="overlay"></div>
        <?php wp_footer(); ?>
    </body>
</html>