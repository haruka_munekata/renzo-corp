            <?php get_header(); ?>
            <main id="main">
                <section class="sec02">
                    <h2 class="ttl02">404 Not found.</h2>
                    <div>
                    <p>お探しのページは移動したか、削除された可能性がございます。</p>
                    </div>
                </section>
                <?php get_footer(); ?>