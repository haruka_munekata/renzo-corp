<?php
/*
Template Name: 特別ページ01
*/
?>
<?php get_header(); ?>
                <article class="sec01">
                    <h2 class="ttl02"><?php echo get_the_title(); ?></h2>
                    <div class="slide-img-area">
	                    <div class="slideShow">
	                    	<div class="mainView">
	                    		<ul>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/01.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/02.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/03.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/01.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/02.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/03.png" alt=""></li>
	                    		</ul>
	                    	</div><!--/.mainView-->
	                    	<div class="thumbNail">
	                    		<ul>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/01.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/02.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/03.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/01.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/02.png" alt=""></li>
	                    			<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/special/sample/03.png" alt=""></li>
	                    		</ul>
	                    	</div><!--/.thumbNail-->

	                    </div><!--/.slideShow-->
                    </div>
                </article>
                <article class="sec02">
                        <?php
                        if(have_posts()): while(have_posts()): the_post();?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                </article>
                <article class="sec03">
	                <div class="special_contents_area">
		                <div class="img">
		                	<img src="http://placekitten.com/320/400" alt="">
		                </div>
		                <div class="text">
				<h5 class="ttl05">小見出し</h5>
		                	<p>テキストが入りますテキストが入りますテキストが入ります</p>
		                	<p>テキストが入りますテキストが入りますテキストが入ります</p>
		                	<p>テキストが入りますテキストが入りますテキストが入ります</p>
		                </div>
	                </div>
                </article>
<?php get_footer(); ?>