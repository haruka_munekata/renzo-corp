<?php get_header(); ?>
                <article class="sec01">
                    <h2 class="ttl02"><?php echo get_the_title(); ?></h2>
                    <ul class="sitemaplist">
                      <li><a href="/">HOME</a></li>
                      <li><a href="/blog/">BLOG</a></li>
                      <li><a href="/collections/">COLLECTIONS</a></li>
                      <li><a href="/access/">ACCESS</a></li>
                      <li><a href="/company/">COMPANY</a></li>
                      <li><a href="/privacypolicy/">PRIVACY</a></li>
                      <li><a href="/sitemap/">SITE MAP</a></li>
                    </ul>
                </article>
<?php get_footer(); ?>