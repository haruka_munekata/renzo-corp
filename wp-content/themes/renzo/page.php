<?php
/*
Template Name: simplepage
*/
?>
<?php get_header(); ?>
                <article class="sec01">
                    <h2 class="ttl02"><?php echo get_the_title(); ?></h2>
                        <?php
                        if(have_posts()): while(have_posts()): the_post();?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                </article>
<?php get_footer(); ?>