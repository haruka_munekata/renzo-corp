jQuery(function(){
	// ページのフェードインとフェードアウト
	document.body.style.display = 'none';
            $('body').fadeIn(1000);
            $(window).on("beforeunload",function(e){
                $('body').fadeOut();
            });
	// #で始まるアンカーをクリックした場合に処理
	jQuery('a[href^=#]').click(function() {
		// スクロールの速度
		var speed = 400; // ミリ秒
		// アンカーの値取得
		var href= jQuery(this).attr("href");
		// 移動先を取得
		var target = jQuery(href == "#" || href == "" ? 'html' : href);
		// 移動先を数値で取得
		var position = target.offset().top;
		// スムーススクロール
		jQuery('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
	jQuery('.hamburger-button').click(function() {
		jQuery(this).toggleClass('active');
		jQuery('#g-nav').toggleClass('active');
		jQuery('#overlay').toggleClass('active');
		jQuery('body').toggleClass('ray-hidden');
	});

	jQuery('.sc').css('visibility','hidden');
	jQuery(window).scroll(function(){
		var windowHeight = jQuery(window).height(),
		topWindow = jQuery(window).scrollTop();
		jQuery('.sc').each(function(){
			var targetPosition = jQuery(this).offset().top;
			if(topWindow > targetPosition - windowHeight + 10){
				jQuery(this).addClass("fadeInDown");
			}
		});
	});
	$(document).ready(function(){
	    $('#slider').bxSlider({
	    	mode: 'fade',
	    	pager: false,
	    	controls: false,
	    	auto: true,
	    	speed: 2000,
	    });
	  });

	// 特別スライドショー
	   jQuery(window).load(function(){
	        var setWrap =jQuery('.slideShow'),
	        setMainView =jQuery('.mainView'),
	        setThumbNail =jQuery('.thumbNail'),
	        setMaxWidth = 1024,
	        setMinWidth = 320,
	        thumbNum = 6,
	        thumbOpc = 0.5,
	        fadeTime = 1000,
	        delayTime = 5000,
	        sideNavi = 'on', // 'on' or 'off'
	        autoPlay = 'on'; // 'on' or 'off'

	        setWrap.each(function(){
	            var thisObj =jQuery(this),
	            childMain = thisObj.find(setMainView),mainUl = childMain.find('ul'),mainLi = mainUl.find('li'),mainLiFst = mainUl.find('li:first'),
	            childThumb = thisObj.find(setThumbNail),thumbUl = childThumb.find('ul'),thumbLi = childThumb.find('li'),thumbLiFst = childThumb.find('li:first'),thumbLiLst = childThumb.find('li:last');

	            thisObj.css({width:setMaxWidth,display:'block'});

	            mainLi.each(function(i){
	               jQuery(this).attr('class','view' + (i + 1).toString()).css({zIndex:'98',opacity:'0'});
	                mainLiFst.css({zIndex:'99'}).stop().animate({opacity:'1'},fadeTime);
	            });

	            thumbLi.click(function(){
	                if(autoPlay == 'on'){clearInterval(setTimer);}

	                var connectCont = thumbLi.index(this);
	                var showCont = connectCont+1;
	                mainUl.find('.view' + (showCont)).siblings().stop().animate({opacity:'0'},fadeTime,function(){jQuery(this).css({zIndex:'98'});});
	                mainUl.find('.view' + (showCont)).stop().animate({opacity:'1'},fadeTime,function(){jQuery(this).css({zIndex:'99'});});

	               jQuery(this).addClass('active');
	               jQuery(this).siblings().removeClass('active');

	                if(autoPlay == 'on'){timer();}

	            });
	            thumbLi.css({opacity:thumbOpc});
	            thumbLiFst.addClass('active');

	            // メイン画像をベースにエリアの幅と高さを設定
	            var mainLiImg = mainLi.find('img'),
	            baseWidth = mainLiImg.width(),
	            baseHeight = mainLiImg.height();

	            // レスポンシブ動作メイン
	            imgSize();
	            function imgSize(){
	                var windowWidth = parseInt(jQuery(window).width());
	                if(windowWidth >= setMaxWidth) {
	                    thisObj.css({width:setMaxWidth});
	                    childMain.css({width:baseWidth,height:baseHeight});
	                    mainUl.css({width:baseWidth,height:baseHeight});
	                    mainLi.css({width:baseWidth,height:baseHeight});
	                    thumbLi.css({width:((setMaxWidth)/(thumbNum))});
	                } else if(windowWidth < setMaxWidth) {
	                    if(windowWidth >= setMinWidth) {
	                        thisObj.css({width:'100%'});
	                        childMain.css({width:'100%'});
	                        mainUl.css({width:'100%'});
	                        mainLi.css({width:'100%'});
	                    } else if(windowWidth < setMinWidth) {
	                        thisObj.css({width:setMinWidth});
	                        childMain.css({width:setMinWidth});
	                        mainUl.css({width:setMinWidth});
	                        mainLi.css({width:setMinWidth});
	                    }
	                    var reHeight = mainLiImg.height();
	                    childMain.css({height:reHeight});
	                    mainUl.css({height:reHeight});
	                    mainLi.css({height:reHeight});

	                    var reWidth = setThumbNail.width();
	                    thumbLi.css({width:((reWidth)/(thumbNum))});
	                }
	            }
	           jQuery(window).resize(function(){imgSize();});
	            imgSize();

	            // サムネイルマウスオーバー
	            var agent = navigator.userAgent;
	            if(!(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1)){
	                thumbLi.hover(function(){
	                   jQuery(this).stop().animate({opacity:'1'},200);
	                },function(){
	                   jQuery(this).stop().animate({opacity:thumbOpc},200);
	                });
	            }

	            // ボタン移動動作
	            function switchNext(){
	                var setActive = thumbUl.find('.active');
	                setActive.each(function(){
	                    var listLengh = thumbLi.length,
	                    listIndex = thumbLi.index(this),
	                    listCount = listIndex+1;

	                    if(listLengh == listCount){
	                        thumbLiFst.click();
	                    } else {
	                       jQuery(this).next('li').click();
	                    }
	                });
	            }
	            function switchPrev(){
	                var setActive = thumbUl.find('.active');
	                setActive.each(function(){
	                    var listLengh = thumbLi.length,
	                    listIndex = thumbLi.index(this),
	                    listCount = listIndex+1;

	                    if(1 == listCount){
	                        thumbLiLst.click();
	                    } else {
	                       jQuery(this).prev('li').click();
	                    }
	                });
	            }

	            // サイドナビボタン（有り無し）
	            if(sideNavi == 'on'){
	                childMain.append('<a href="javascript:void(0);" class="btnPrev"></a><a href="javascript:void(0);" class="btnNext"></a>');
	                var btnPrev = thisObj.find('.btnPrev'),btnNext = thisObj.find('.btnNext'),btnPrevNext = thisObj.find('.btnPrev,.btnNext');
	                if(!(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1)){
	                    btnPrevNext.css({opacity:thumbOpc}).hover(function(){
	                       jQuery(this).stop().animate({opacity:'0.9'},200);
	                    },function(){
	                       jQuery(this).stop().animate({opacity:thumbOpc},200);
	                    });
	                } else {
	                    btnPrevNext.css({opacity:thumbOpc});
	                }
	                btnPrev.click(function(){switchPrev();});
	                btnNext.click(function(){switchNext();});
	            }

	            // 自動再生（有り無し）
	            if(autoPlay == 'on'){
	                function timer(){
	                    setTimer = setInterval(function(){
	                        switchNext();
	                    },delayTime);
	                }
	                timer();
	            }
	        });
	    });
});