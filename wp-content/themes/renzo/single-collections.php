<?php get_header(); ?>
                <?php
                global $wp_query;
                query_posts(array_merge(
                    array( 'post_type' => array('post','collections') ),
                    $wp_query->query
                ));
                if (have_posts()) : while (have_posts()) :
                the_post();
                ?>
                <section class="sec01">
                    <h2 class="ttl02"><?php the_title(); ?></h2>
                    <div class="eyecatch">
                        <?php if (has_post_thumbnail()) : {the_post_thumbnail( 'full' );}?>
                        <?php else : ?>
                        <img src="http://dummyimage.com/640x800/ccc/999" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="text">
                        <p><?php the_field( 'overview' ); ?></p>
                    </div>
                </section>
                <section class="sec02">
                    <p><?php the_content(); ?></p>
                </section>
                <?php
                endwhile;
                endif;
                ?>
<?php get_footer(); ?>