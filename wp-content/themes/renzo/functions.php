<?php
// 必要なスクリプトを読み込みます。
function renzo_scripts() {
  // css
  wp_enqueue_style('renzo_style', get_stylesheet_uri());

  // js
  wp_enqueue_script('jquery');
  wp_enqueue_script('renzo_script', get_template_directory_uri() . '/components/js/common.js');
}
add_action('wp_enqueue_scripts', 'renzo_scripts');


function head_scripts() {
echo <<<EOC

  <!--[if lt IE 9]>
    <script src="http://guruglewebdesign.com/wp-content/themes/guruglecosmos/components/js/html5shiv.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->
  <!--[if lt IE 8]>
    <div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display='none'; return false;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>      <div style='width: 75px; float: left;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning!'/></div>      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>        <div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>あなたは旧式ブラウザをご利用中です</div>        <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>このウェブサイトを快適に閲覧するにはブラウザをアップグレードしてください。</div>      </div>      <div style='width: 75px; float: left;'><a href='http://www.mozilla.jp' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>      <div style='width: 75px; float: left;'><a href='http://www.microsoft.com/downloads/details.aspx?FamilyID=341c2ad5-8c3d-4347-8c03-08cdecd8852b&DisplayLang=ja' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>      <div style='width: 73px; float: left;'><a href='http://www.apple.com/jp/safari/download/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>      <div style='float: left;'><a href='http://www.google.com/chrome?hl=ja' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>    </div>  </div>
  <![endif]-->

EOC;
}
add_action('wp_head', 'head_scripts');

// ビジュアルリッチエディタを非表示
add_filter('user_can_richedit' , create_function('' , 'return false;') , 50);

// 自動挿入される<p></p>を削除します。
remove_filter('the_excerpt', 'wpautop');

// ヘルプの非表示
function mytheme_remove_help_tabs($old_help, $screen_id, $screen){
  $screen->remove_help_tabs();
  return $old_help;
}
add_filter( 'contextual_help', 'mytheme_remove_help_tabs', 999, 3 );

// adminbarをカスタマイズします。
function custom_admin_bar($wp_admin_bar) {
  $wp_admin_bar->remove_node( 'customize' );
  if (is_page()) {
  $wp_admin_bar->remove_node( 'edit' );
}

$wp_admin_bar->add_menu( array(
  'id' => 'my-logout',
  'title' => 'ログアウト',
  'href' => wp_logout_url()
) );

if (! is_admin()) {
  $wp_admin_bar->add_menu( array(
    'id' => 'front-and-back',
    'title' => '管理画面へ',
    'href' => get_admin_url() . 'edit.php'
  ) );
  } else {
  $wp_admin_bar->add_menu( array(
    'id' => 'front-and-back',
    'title' => 'サイトを表示する',
    'href' => home_url()
    ) );
  }
}
add_action( 'admin_bar_menu', 'custom_admin_bar', 9999 );

// 使用しないメニューを非表示にする
function remove_admin_menus() {
    //if (!current_user_can('level_10')) {
    global $menu;
    //unsetで非表示にするメニューを指定
    unset($menu[5]);        // 投稿
    unset($menu[25]);       // コメント
    unset($menu[60]);       // 外観
    unset($menu[75]);       // ツール
    //}
}
add_action('admin_menu', 'remove_admin_menus');

// headタグからバージョン情報を削除します。
remove_action( 'wp_head', 'wp_generator' );

// 投稿でアイキャッチが使えるようにします。
add_theme_support( 'post-thumbnails' );

//カスタム投稿タイプ：コレクション
add_action( 'init', 'renzo_collections' );
function renzo_collections() {
    //表示設定
    $labels = array(
        'name' => _x( 'COLLECTIONS', 'collections' ),//一覧の投稿タイプ名
        'menu_name' => _x( 'collections', 'collections' ),//メニューの投稿タイプ名
        'edit_item' => __('記事を編集'),
        'search_items' => __('COLLECTIONSを検索'),
        'view_item' => __('プレビュー'),
    );
    //オプション設定
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,

        'supports' => array( 'title', 'editor', 'revisions', 'thumbnail' /* 'custom-fields'*/ ),
        //'taxonomies' => array( 'category', 'post_tag', 'page-category'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,//投稿の下に表示

        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front' => false),
        'capability_type' => 'post'//投稿として利用
    );
    //カスタム投稿タイプを登録する
    register_post_type( 'collections', $args );
}

//カスタム投稿タイプ：ブログ
add_action( 'init', 'renzo_blog' );
function renzo_blog() {
    //表示設定
    $labels = array(
        'name' => _x( 'BLOG', 'blog' ),//一覧の投稿タイプ名
        'menu_name' => _x( 'blog', 'blog' ),//メニューの投稿タイプ名
        'edit_item' => __('記事を編集'),
        'search_items' => __('blogを検索'),
        'view_item' => __('プレビュー'),
    );
    //オプション設定
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,

        'supports' => array( 'title', 'editor', 'revisions', 'thumbnail' /*'custom-fields'*/ ),
        //'taxonomies' => array( 'category', 'post_tag', 'page-category'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 6,//投稿の下に表示

        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front' => false),
        'capability_type' => 'post'//投稿として利用
    );
    //カスタム投稿タイプを登録する
    register_post_type( 'blog', $args );
}

// ログインページのロゴを任意のロゴに変更する
function custom_login_logo() {
echo '<style type="text/css">h1 a { background: url('.get_bloginfo('template_url').'/components/img/common/login_logo.png) 50% top no-repeat !important; background-size: 175px 55px important; width:175px !important; height: 90px !important; } </style>';
}
add_action('login_head', 'custom_login_logo');

// ログインページのロゴのURLを変更する
function custom_login_logo_link(){
return get_bloginfo('home').'/';
}
add_filter('login_headerurl','custom_login_logo_link');