<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="ja" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]> <html lang="ja" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="ja" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="ja" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
    <head prefix="og: http://ogp.me/ns#">
        <link rel="canonical" href="">
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
        <meta content="" name="description">
        <!-- External files -->
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/components/css/set1.css" rel="stylesheet">
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/components/css/jquery.bxslider.min.css" rel="stylesheet">
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/components/js/jquery-1.11.2.min.js"></script>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/components/js/modernizr.custom.js"></script>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/components/js/jquery.bxslider.min.js"></script>
        <!-- Favicon, Thumbnail image -->
        <link href="<?php echo home_url() ?>/img/favicon.ico" rel="shortcut icon">
        <link href="<?php echo home_url() ?>/img/favicon_big.png" rel="apple-touch-icon">
        <!-- OGP -->
        <meta content="renzo &amp; fabio - 鎌倉・由比ヶ浜発信のアパレルファッションブランドです。" property="og:title">
        <meta content="" property="og:description">
        <meta content="http://www.renzo.co.jp" property="og:url">
        <meta content="http://" property="og:image">
        <meta content="website" property="og:type">
        <meta content="renzo &amp; fabio" property="og:site_name">
        <noscript><style>.noscript{display:block;}</style></noscript>
        <?php wp_head(); ?>
    </head>
    <body>
        <p class="noscript">このWebサイトを正常にご覧いただくには、お使いのブラウザのJavaScriptを有効にする必要があります。</p>
        <div id="wrapper" <?php if (is_home()) { body_class(); } elseif (is_archive()) { body_class($wp_query->query_vars['post_type']); } else { body_class($name); } ?>>
            <div id="container">
            <header id="header">
                <a class="logo" href="/"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/common/logo.png" alt="renzo&amp;fabio"></a>
                <div class="cta"><a href="https://www.facebook.com/pages/Renzo-%E9%8E%8C%E5%80%89/515917995225706" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/common/btn_fb.png" alt="facebookへのリンク"></a></div>
                <button class="hamburger-button">
                    <div class="bar01"></div>
                    <div class="bar02"></div>
                    <div class="bar03"></div>
                </button>
                <nav id="g-nav" class="cl-effect-1">
                    <a href="<?php echo home_url(); ?>/">HOME</a>
                    <a href="<?php echo home_url(); ?>/blog/">BLOG</a>
                    <a href="<?php echo home_url(); ?>/collections/">COLLECTIONS</a>
                    <a href="<?php echo home_url(); ?>/access/">ACCESS</a>
                    <a href="<?php echo home_url(); ?>/company/">COMPANY</a>
                </nav>
            </header>
            <!-- /#header -->
            <?php if(! is_home()) : ?>
            <ul id="breadcrumbs">
                <?php if( function_exists('bcn_display') ) { bcn_display(); }?>
            </ul>
            <main id="main">
            <?php endif; ?>