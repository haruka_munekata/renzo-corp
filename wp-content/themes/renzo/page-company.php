<?php get_header(); ?>
                <article class="sec01">
                    <h2 class="ttl02"><?php echo get_the_title(); ?></h2>
                    <table class="tb01">
                        <tbody>
                            <tr>
                                <th>会社名</th>
                                <td>株式会社 ヴェントマリノ</td>
                            </tr>
                            <tr>
                                <th>所在地</th>
                                <td>〒248-0014 神奈川県鎌倉市由比ヶ浜3-9-12</td>
                            </tr>
                            <tr>
                                <th>電話番号</th>
                                <td>0467-23-1531</td>
                            </tr>
                            <tr>
                                <th>FAX</th>
                                <td>0467-23-1531</td>
                            </tr>
                            <tr>
                                <th>代表者名</th>
                                <td>松本 弘子</td>
                            </tr>
                            <tr>
                                <th>業務内容</th>
                                <td>ブランドの企画・製造・販売<br>ブランドディレクション</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="tile">
                        <?php
                        if(have_posts()): while(have_posts()): the_post();?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                </article>
<?php get_footer(); ?>