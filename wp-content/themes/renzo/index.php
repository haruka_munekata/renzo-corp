            <?php get_header(); ?>
            <section class="sec01 tac">
                <div id="billboard">
                    <div id="slider">
                        <div>
                            <img class="sp-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/02.png" alt="おすすめシャツの画像">
                            <img class="pc-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/01.png" alt="おすすめシャツの画像">
                        </div>
                        <div>
                            <img class="pc-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/03.png" alt="おすすめシャツの画像">
                            <img class="sp-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/05.png" alt="おすすめシャツの画像">
                        </div>
                        <div>
                            <img class="pc-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/04.png" alt="おすすめシャツの画像">
                            <img class="sp-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/06.png" alt="おすすめシャツの画像">
                        </div>
                        <div>
                            <img class="pc-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/03.png" alt="おすすめシャツの画像">
                            <img class="sp-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/07.png" alt="おすすめシャツの画像">
                        </div>
                    </div>
                    <div class="kame"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/components/img/home/kame.png" alt=""></div>
                </div>
                <h2 class="ttl03-a">renzo&amp;fabio</h2>
                <p class="subtitle">KAMAKURA</p>
                <p class="mb1">鎌倉・由比ヶ浜・小町を拠点にした<br>アパレルブランドショップrenzoでは<br>やさしい自然からの贈り物をテーマに<br>「ゆるやかな時」を提案しています。</p>
                <p class="shop_en_fonts">In 2002<br>renzo&amp;fabio opened a small shop in Kamakura.<br>We started with producing small items that has a lot of warm quality. <br>It marks our 15th anniversary in 2017.<br>We want to continue to produce heartful items and looking forward to share with all customors.</p>
                <a href="<?php echo get_home_url(); ?>/access/" class="btn01" style="margin-bottom: 2em;">ACCESS<span></span></a>
                <p class="mb1">instagram始めました。<br>新作や、鎌倉のことをアップしています。</p>
                <a href="https://www.instagram.com/renzoandfabio/" class="btn01" target="_blank">INSTAGRAM<span></span></a>
            </section>
            <main id="main">
                <section class="sec02">
                    <h2 class="ttl02">renzo BLOG</h2>
                    <div>
                        <?php query_posts(array('post_type'=>'blog','posts_per_page'=>'2'));?>
                        <?php if(have_posts()):while(have_posts()):the_post();?>
                        <article class="blogarea sc">
                            <a href="<?php the_permalink();?>">
                                <span class="imgdiv">
                                    <?php if (has_post_thumbnail()) : {the_post_thumbnail( 'full' );}?>
                                    <?php else : ?>
                                    <img src="http://dummyimage.com/600x400/ccc/999" alt="">
                                    <?php endif; ?>
                                </span>
                                <div>
                                    <span class="bdl">
                                        <h3 class="ttl03"><?php echo get_the_title();?></h3>
                                        <p><?php the_field( 'more' ); ?></p>
                                    </span>
                                </div>
                            </a>
                        </article>
                        <?php endwhile;?>
                        <?php endif;?>
                    </div>
                    <div class="supplement">
                        <p>renzoブログでは、新商品のお知らせや予約会のご案内など、最新の情報をご覧いただけます。</p>
                        <a href="<?php echo get_home_url(); ?>/blog/" class="btn01">BLOG<span class=""></span></a>
                    </div>
                </section>
                <?php get_footer(); ?>